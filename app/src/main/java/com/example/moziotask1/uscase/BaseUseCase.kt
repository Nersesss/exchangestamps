package com.example.moziotask1.uscase

interface BaseUseCase<out Output, in Parameters> {

    suspend fun invoke(parameters: Parameters): Output

}