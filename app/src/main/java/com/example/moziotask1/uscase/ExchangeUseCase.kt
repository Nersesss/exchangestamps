package com.example.moziotask1.uscase

import com.example.moziotask1.repository.ExchangeRepository
import javax.inject.Inject

class ExchangeUseCase @Inject constructor(
    private val repository: ExchangeRepository
) : BaseUseCase<Result<Pair<IntArray, IntArray>>, Pair<IntArray, IntArray>> {

    override suspend fun invoke(parameters: Pair<IntArray, IntArray>): Result<Pair<IntArray, IntArray>> {
        return Result.success(repository.getExchangePair(parameters.first, parameters.second))
    }
}