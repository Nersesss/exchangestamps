package com.example.moziotask1.extension

fun String.toIntArray() = this.map { "$it".toInt() }.toIntArray()

fun String.getNumbers() = this.replace("[^+0-9]".toRegex(), "")
