package com.example.moziotask1.di

import com.example.moziotask1.repository.ExchangeRepository
import com.example.moziotask1.repository.ExchangeRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(includes = [AppModule.Declarations::class])
@InstallIn(SingletonComponent::class)
class AppModule {

    @Module
    @InstallIn(SingletonComponent::class)
    abstract class Declarations {
        @Binds
        abstract fun provideExchangeRepository(repository: ExchangeRepositoryImpl): ExchangeRepository
    }
}
