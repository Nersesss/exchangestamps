package com.example.moziotask1.ui.exchange

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.moziotask1.extension.toIntArray
import com.example.moziotask1.ui.theme.MozioTask1Theme

@Composable
fun ExchangeScreen(
    viewModel: ExchangeViewModel
) {

    val state by viewModel.state.collectAsState()
    var isCalculatedState by remember { mutableStateOf(false) }

    ExchangeScreenContent(
        firstPersonTextFieldValue = state.firstPersonTextFieldValue,
        secondPersonTextFieldValue = state.secondPersonTextFieldValue,
        stampsToExchange = state.stampsToExchange,
        onClick = {
            isCalculatedState = true
            viewModel.calculateExchangeStamps()
        },
        onFirstPersonTextChange = {
            viewModel.onFirstPersonTextChange(it)
        },
        onSecondPersonTextChange = {
            viewModel.onSecondPersonTextChange(it)
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun ExchangeScreenContent(
    stampsToExchange: Pair<IntArray, IntArray>,
    onClick: () -> Unit = {},
    firstPersonTextFieldValue: TextFieldValue = TextFieldValue(),
    secondPersonTextFieldValue: TextFieldValue = TextFieldValue(),
    onFirstPersonTextChange: (TextFieldValue) -> Unit = {},
    onSecondPersonTextChange: (TextFieldValue) -> Unit = {},

    ) {
    val keyboardController = LocalSoftwareKeyboardController.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 24.dp)
            .verticalScroll(rememberScrollState())
    ) {
        Text(
            text = "Jane,please enter your stamps with comma",
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = 8.dp)
        )
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 32.dp),
            value = firstPersonTextFieldValue,
            onValueChange = onFirstPersonTextChange,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    keyboardController?.hide()
                    onClick.invoke()
                }
            )
        )

        Text(
            text = "Alice enter your stamps",
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = 8.dp)
        )

        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 32.dp),
            value = secondPersonTextFieldValue,
            onValueChange = onSecondPersonTextChange,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    keyboardController?.hide()
                    onClick.invoke()
                }
            )
        )

        Text(
            text = "Jane your stamps to exchange is \n      ${
                if (stampsToExchange.first.isEmpty()) "empty"
                else stampsToExchange.first.joinToString { it.toString() }
            }",
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp, start = 8.dp)
        )
        Text(
            text = "Alice your stamps to exchange is \n      ${
                if (stampsToExchange.first.isEmpty()) "empty"
                else stampsToExchange.second.joinToString { it.toString() }
            }",
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp, start = 8.dp)
        )

        Spacer(modifier = Modifier.weight(1f))

        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .navigationBarsPadding(),
            onClick = onClick
        ) {
            Text(text = "Calculate")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ExchangeScreenContentPreview() {
    MozioTask1Theme {
        ExchangeScreenContent(
            stampsToExchange = intArrayOf() to intArrayOf(),
        )
    }
}
