package com.example.moziotask1.ui.exchange

import androidx.compose.ui.text.input.TextFieldValue
import com.example.moziotask1.ui.base.ViewEffect
import com.example.moziotask1.ui.base.ViewEvent
import com.example.moziotask1.ui.base.ViewState
import javax.annotation.concurrent.Immutable

@Immutable
sealed class ExchangeViewEvent : ViewEvent {
    data class UpdateStampsToExchange(
        val stampsToExchange: Pair<IntArray, IntArray>
    ) : ExchangeViewEvent()

    data class OnFirstPersonTextChange(
        val firstPersonTextFieldValue: TextFieldValue
    ) : ExchangeViewEvent()

    data class OnSecondPersonTextChange(
        val secondPersonTextFieldValue: TextFieldValue
    ) : ExchangeViewEvent()
}

@Immutable
sealed class ExchangeEffect : ViewEffect

@Immutable
data class ExchangeViewState(
    val stampsToExchange: Pair<IntArray, IntArray>,
    val firstPersonTextFieldValue: TextFieldValue,
    val secondPersonTextFieldValue: TextFieldValue
) : ViewState {
    companion object {

        fun initial() = ExchangeViewState(
            stampsToExchange = intArrayOf() to intArrayOf(),
            firstPersonTextFieldValue = TextFieldValue("1731745171"),
            secondPersonTextFieldValue = TextFieldValue("2332432"),
        )
    }
}
