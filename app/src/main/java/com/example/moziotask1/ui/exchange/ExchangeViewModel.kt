package com.example.moziotask1.ui.exchange

import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.viewModelScope
import com.example.moziotask1.uscase.ExchangeUseCase
import com.example.moziotask1.extension.getNumbers
import com.example.moziotask1.extension.toIntArray
import com.example.moziotask1.ui.base.BaseViewModel
import com.example.moziotask1.ui.base.Reducer
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ExchangeViewModel @Inject constructor(private val calculateExchangeUseCase: ExchangeUseCase) :
    BaseViewModel<ExchangeViewState, ExchangeViewEvent, ExchangeEffect>() {

    private val reducer = ExchangeReducer(ExchangeViewState.initial())

    override val state: StateFlow<ExchangeViewState>
        get() = reducer.state
    override val event: SharedFlow<ExchangeViewEvent>
        get() = reducer.event
    override val effect: Flow<ExchangeEffect>
        get() = reducer.effect

    private fun sendEvent(event: ExchangeViewEvent) = reducer.sendEvent(event)

    init {
        calculateExchangeStamps()
    }

    fun calculateExchangeStamps() {
        viewModelScope.launch(IO) {
            val result =
                calculateExchangeUseCase.invoke(
                    state.value.firstPersonTextFieldValue.text.getNumbers().toIntArray()
                            to state.value.secondPersonTextFieldValue.text.getNumbers().toIntArray()
                )
            result.onSuccess { stampsToExchange ->
                sendEvent(
                    event = ExchangeViewEvent.UpdateStampsToExchange(
                        stampsToExchange = stampsToExchange
                    )
                )
            }
            result.onFailure {
                // TODO not implemented yet
            }
        }
    }

    fun onFirstPersonTextChange(textFieldValue: TextFieldValue) {
        sendEvent(event = ExchangeViewEvent.OnFirstPersonTextChange(textFieldValue))
    }

    fun onSecondPersonTextChange(textFieldValue: TextFieldValue) {
        sendEvent(event = ExchangeViewEvent.OnSecondPersonTextChange(textFieldValue))
    }

    class ExchangeReducer(initial: ExchangeViewState) :
        Reducer<ExchangeViewState, ExchangeViewEvent, ExchangeEffect>(initial) {
        override fun reduce(oldState: ExchangeViewState, event: ExchangeViewEvent) {
            when (event) {
                is ExchangeViewEvent.UpdateStampsToExchange ->
                    setState(newState = oldState.copy(stampsToExchange = event.stampsToExchange))

                is ExchangeViewEvent.OnFirstPersonTextChange ->
                    setState(
                        newState = oldState.copy(
                            firstPersonTextFieldValue = event.firstPersonTextFieldValue
                        )
                    )

                is ExchangeViewEvent.OnSecondPersonTextChange ->
                    setState(
                        newState = oldState.copy(
                            secondPersonTextFieldValue = event.secondPersonTextFieldValue
                        )
                    )
            }
        }
    }
}