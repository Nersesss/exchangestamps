package com.example.moziotask1

import android.annotation.SuppressLint
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.moziotask1.ui.exchange.ExchangeScreen
import com.example.moziotask1.ui.exchange.ExchangeViewModel
import com.example.moziotask1.ui.theme.MozioTask1Theme

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ExchangeApp() {
    val navController = rememberNavController()

    MozioTask1Theme {
        Scaffold(content = {
            NavHost(navController = navController, startDestination = Route.ExchangeScreen) {
                exchangeScreenRoute()
            }
        })
    }
}

private fun NavGraphBuilder.exchangeScreenRoute() {
    composable(Route.ExchangeScreen) {
        val viewModel = hiltViewModel<ExchangeViewModel>()
        ExchangeScreen(viewModel = viewModel)
    }
}
