package com.example.moziotask1.repository

interface ExchangeRepository {
    suspend fun getExchangePair(
        firstPersonStamps: IntArray,
        secondPersonStamps: IntArray
    ): Pair<IntArray, IntArray>
}
