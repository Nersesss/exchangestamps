package com.example.moziotask1.repository

import javax.inject.Inject

const val minExchangeableValue = 2

class ExchangeRepositoryImpl @Inject constructor() : ExchangeRepository {

    override suspend fun getExchangePair(
        firstPersonStamps: IntArray,
        secondPersonStamps: IntArray
    ): Pair<IntArray, IntArray> {
        val firstPersonStampsMap = getCountByStamps(firstPersonStamps)
        val secondPersonStampsMap = getCountByStamps(secondPersonStamps)

        return Pair(
            getStampsToExchange(secondPersonStampsMap, firstPersonStampsMap),
            getStampsToExchange(firstPersonStampsMap, secondPersonStampsMap)
        )
    }

    private fun getStampsToExchange(
        firstStampsMap: Map<Int, Int>,
        secondStampsMap: Map<Int, Int>
    ): IntArray {
        val stampsToExchange = mutableListOf<Int>()

        secondStampsMap.forEach { (stamp, count) ->
            if ((!firstStampsMap.containsKey(stamp) || firstStampsMap[stamp] == 1) && count > minExchangeableValue) {
                repeat(count - minExchangeableValue) {
                    stampsToExchange.add(stamp)
                }
            }
        }
        return stampsToExchange.toIntArray()
    }

    private fun getCountByStamps(stamps: IntArray): Map<Int, Int> {
        val countByStampsMap = mutableMapOf<Int, Int>()
        stamps.forEach {
            countByStampsMap[it] = countByStampsMap.getOrDefault(it, 0) + 1
        }
        return countByStampsMap
    }
}
